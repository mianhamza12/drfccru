from django.contrib import admin
from .models import student_db,general_db
# Register your models here.
@admin.register(student_db)
class show(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name','email','password']

@admin.register(general_db)
class show_fu(admin.ModelAdmin):
    list_display=['name','adress','clas']
