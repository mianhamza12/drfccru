from django.db import models

# Create your models here.
class student_db(models.Model):
    username=models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password=models.CharField(max_length=100)


class general_db(models.Model):
    name = models.CharField(max_length=75)
    adress = models.CharField(max_length=75)
    clas = models.CharField(max_length=75)