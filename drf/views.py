from django.shortcuts import render
from rest_framework.decorators import api_view
from .serializer import stuserializer,loginserialization,student_seri
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from .models import general_db
# Create your views here.

@csrf_exempt
@api_view(['POST'])
def regis_fu(request):
    if request.method=='POST':
        fm=stuserializer(data=request.data)
        if fm.is_valid():
            fm.save()
            res={'create':'user create!!!'}
            return Response(res)
        else:
            return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)
@csrf_exempt
@api_view(['POST','GET'])
def login_fu(request):
    if request.method=='POST':
        print(request.data)
        fm = loginserialization(data=request.data)
        if fm.is_valid():
            username=fm.data.get('username')
            password= fm.data.get('password')
            user=authenticate(username=username,password=password)
            if user is not None:
                res={'login':"success!!!!"}
                return Response(res,status=status.HTTP_200_OK)
            else:
                return Response({'errors':{'non_field_errors':['username or password is not correct']}})
        else:
            return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST','GET','PUT',"DELETE"])
def general_fu(request):
    if request.method=='POST':
        fb=student_seri(data=request.data)
        if fb.is_valid():
            fb.save()
            res = {'DATA': "data is inserted !!!!"}
            return Response(res,status=status.HTTP_200_OK)
        else:
            return Response(fb.errors, status=status.HTTP_400_BAD_REQUEST)


    if request.method=='GET':
        stu=general_db.objects.all()
        fm=student_seri(stu,many=True)
        return Response(fm.data,status=status.HTTP_200_OK)

    if request.method=='PUT':
        id=request.data.get('id')
        stu=general_db.objects.get(pk=id)
        fm=student_seri(stu,data=request.data,partial=True)
        if fm.is_valid():
            fm.save()
            res={'update':'data update!!!'}
            return Response(res,status=status.HTTP_200_OK)
        else:
            return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)

    if request.method=="DELETE":
        id=request.data.get('id')
        stu=general_db.objects.get(pk=id)
        stu.delete()
        res={'delete':'data delete!!!!'}
        return Response(res)





